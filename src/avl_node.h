// Copyright (C) 2020 Andrew Todd
#ifndef CPT_S_223_PA3_SRC_AVL_NODE_H_
#define CPT_S_223_PA3_SRC_AVL_NODE_H_

#include <string>

#include "cancer_data.h"  // NOLINT

template <typename T>
class AVLTree;

template <typename T>
class AVLNode {
 private:
  T value;
  AVLNode *left = nullptr;
  AVLNode *right = nullptr;
  int height = 0;

 public:
  explicit AVLNode(T data) {
    this->value = data;
    this->left = nullptr;
    this->right = nullptr;
  }

  AVLNode(T data, AVLNode<T>* setLeft, AVLNode<T>* setRight) {
    this->value = data;
    this->left = setLeft;
    this->right = setRight;
  }

  ~AVLNode() {
    this->left = nullptr;
    this->right = nullptr;
  }
  friend class AVLTree<T>;
};

#endif  // CPT_S_223_PA3_SRC_AVL_NODE_H_
