// Copyright (C) 2020 Andrew Todd
#include "cancer_data.h"  // NOLINT

#include <string>
#include <iostream>

// default initialization
CancerData::CancerData() {
  // NOLINT
}

// Initialize with some data
CancerData::CancerData(std::string in_country, double in_rate) {
  // O(2)
  country_name_ = in_country;
  cancer_rate_ = in_rate;
}

// Copy constructor
CancerData::CancerData(const CancerData &copy) {
  // O(2)
  cancer_rate_ = copy.cancer_rate_;
  country_name_ = copy.country_name_;
}

CancerData& CancerData::operator =(const CancerData &copy_op) {
  // O (3)
  if (this != &copy_op) {
    cancer_rate_ = copy_op.cancer_rate_;
    country_name_ = copy_op.country_name_;
  }
  return *this;
}

bool operator >(const CancerData &op1, const CancerData &op2) {
  return op1.cancer_rate_ > op2.cancer_rate_;
}

std::ostream &operator <<(std::ostream &output, const CancerData &out_op) {
  // O(2)
  output << "Country: " << out_op.country_name_;
  output << " Rate: " << out_op.cancer_rate_;
  return output;
}

CancerData::~CancerData() {
  // NOLINT
}
