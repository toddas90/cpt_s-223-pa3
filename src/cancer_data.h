// Copyright (C) 2020 Andrew Todd
#ifndef CPT_S_223_PA3_SRC_CANCER_DATA_H_
#define CPT_S_223_PA3_SRC_CANCER_DATA_H_

#include <string>

class CancerData {
 public:
  CancerData();
  CancerData(std::string country, double rate);
  CancerData(const CancerData &copy);
  CancerData& operator =(const CancerData &copy_op);
  friend bool operator >(const CancerData &op1, const CancerData &op2);
  friend std::ostream &operator <<(std::ostream &output,
                                   const CancerData &out_op);
  ~CancerData();

 private:
  std::string country_name_ = "";
  double cancer_rate_ = 0;
};

#endif  // CPT_S_223_PA3_SRC_CANCER_DATA_H_
