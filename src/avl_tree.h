// Copyright (C) 2020 Andrew Todd
#ifndef CPT_S_223_PA3_SRC_AVL_TREE_H_
#define CPT_S_223_PA3_SRC_AVL_TREE_H_

#include <iostream>
#include <algorithm>
#include <string>

#include "avl_node.h"  // NOLINT
#include "cancer_data.h"  // NOLINT

template <typename T>
class AVLTree {
 protected:
  AVLNode<T> *_root;         // Root of the tree AVLnodes

  // Clone a passed in tree, returns pointer to new tree
  AVLNode<T>* copyTree(AVLNode<T> *t) {
    if (t == nullptr) {
      return nullptr;
    } else {
      return new AVLNode<T>(t->value);
    }
    copyTree(t->left),
    copyTree(t->right);
  }

  // Recursively delete the tree AVLnodes
  void makeClearHelper(AVLNode<T> *t) {
    if (t != nullptr) {
      makeClearHelper(t->left);
      makeClearHelper(t->right);
      delete t;
    }
  }

  // Add new T val to the tree
  void addHelper(AVLNode<T> *&root, T data) {  // NOLINT
    if (root->value > data) {
      if (!root->left) {
        root->left = new AVLNode<T>(data);
      } else {
        addHelper(root->left, data);
      }
    } else {
        if (!root->right) {
        root->right = new AVLNode<T>(data);
      } else {
        addHelper(root->right, data);
      }
    }
    balance(root);
  }

  // Check for balance after insertion
  void balance(AVLNode<T> *&t) {  // NOLINT
    if (t == nullptr) {
      return;
    }
    if (height(t->left) - height(t->right) > 1) {
      if (height(t->left->left) >= height(t->left->right)) {
        rotateLeft(t);
      } else {
        doubleRotateLeft(t);
      }
    } else {
      if (height(t->right) - height(t->left) > 1) {
        if (height(t->right->right) >= height(t->right->left)) {
          rotateRight(t);
        } else {
          doubleRotateRight(t);
        }
      }
    }
    t->height = std::max(height(t->left), height(t->right)) + 1;
  }

  // Rotate left
  void rotateLeft(AVLNode<T> *&t) {  // NOLINT
    AVLNode<T> *temp = t->left;
    t->left = temp->right;
    temp->right = t;
    t->height = std::max(height(t->left), height(t->right)) + 1;
    temp->height = std::max(height(temp->left), t->height) + 1;
    t = temp;
  }

  // Rotate right
  void rotateRight(AVLNode<T> *&t) {  // NOLINT
    AVLNode<T> *temp = t->right;
    t->right = temp->left;
    temp->left = t;
    t->height = std::max(height(t->left), height(t->right)) + 1;
    temp->height = std::max(height(temp->right), t->height) + 1;
    t = temp;
  }

  // Double rotate left
  void doubleRotateLeft(AVLNode<T> *&t) {  // NOLINT
    rotateRight(t->left);
    rotateLeft(t);
  }

  // Double rotate right
  void doubleRotateRight(AVLNode<T> *&t) {  // NOLINT
    rotateLeft(t->right);
    rotateRight(t);
  }

  // Print tree out in inorder (A + B)
  void printInOrderHelper(AVLNode<T> *root, std::ostream& out) {
    if (!root) return;
    printInOrderHelper(root->left, out);
    out << root->value << std::endl;
    printInOrderHelper(root->right, out);
  }

  void printRevOrderHelper(AVLNode<T> *root, std::ostream& out) {
    if (!root) return;
    printRevOrderHelper(root->right, out);
    out << root->value << std::endl;
    printRevOrderHelper(root->left, out);
  }

  // Print tree out in post order (A B +)
  void printPostOrderHelper(AVLNode<T> *root, std::ostream& out) {
    if (!root) return;
    printPostOrderHelper(root->left, out);
    printPostOrderHelper(root->right, out);
    out << root->value << std::endl;
  }

  // Print tree out in pre order (+ A B)
  void printPreOrderHelper(AVLNode<T> *root, std::ostream& out) {
    if (!root) return;
    out << root->value << std::endl;
    printPreOrderHelper(root->left, out);
    printPreOrderHelper(root->right, out);
  }

  void printMaxHelper(AVLNode<T> *root, std::ostream& out) {
    if (!root) return;
    while (root->right != nullptr) {
      root = root->right;
    }
    out << root->value << std::endl;
  }

  void printMinHelper(AVLNode<T> *root, std::ostream& out) {
    if (!root) return;
    while (root->left != nullptr) {
      root = root->left;
    }
    out << root->value << std::endl;
  }

  // Return height of tree (root == nullptr -> 0)
  int heightHelper(AVLNode<T> *root) {
    if (!root) {
      return -1;
    } else {
      return 1 + std::max(heightHelper(root->left), heightHelper(root->right));
    }
  }

  // Print out longest path from root to a leaf
  void printMaxPathHelper(AVLNode<T> *root) {
    if (!root) return;
    std::cout << root->value << ' ';
    if (heightHelper(root->left) > heightHelper(root->right)) {
      printMaxPathHelper(root->left);
    } else {
      printMaxPathHelper(root->right);
    }
  }

  // Delete a given <T> value from tree
  // Does not balance after deletion
  // it was not a requirement and I didn't have time
  bool deleteValueHelper(AVLNode<T> *parent, AVLNode<T>* current, T value) {
    if (!current) return false;
    if (current->value == value) {
      if (current->left == nullptr || current->right == nullptr) {
        AVLNode<T>* temp = current->left;
        if (current->right) temp = current->right;
        if (parent) {
          if (parent->left == current) {
            parent->left = temp;
          } else {
            parent->right = temp;
          }
        } else {
          this->_root = temp;
        }
      } else {
        AVLNode<T>* validSubs = current->right;
        while (validSubs->left) {
          validSubs = validSubs->left;
        }
        T temp = current->value;
        current->value = validSubs->value;
        validSubs->value = temp;
        return deleteValueHelper(current, current->right, temp);
      }
      delete current;
      return true;
    }
    return deleteValueHelper(current, current->left, value) ||
    deleteValueHelper(current, current->right, value);
  }

  bool containsHelper(AVLNode<T> *root, T val) {
    if (root == nullptr) {
      return false;
    } else if (root->value == val) {
      return true;
    } else if (root->value > val) {  // Search left
      return(containsHelper(root->left, val));
    } else {
      return(containsHelper(root->right, val));
    }
  }

 public:
  AVLTree() : _root(nullptr) {
    // Basic initialization constructor
  }

  explicit AVLTree(T data) : _root(nullptr) {
    this->add(data);
  }

  // Destructor - free all AVLnodes in the tree
  ~AVLTree() {
    clear();
  }

  // Copy constructor - perform a "deep" copy
  AVLTree(const AVLTree& other) : _root(nullptr) {
    this->_root = other._root;
    this->copyTree(_root);
  }

  // Move constructor
  AVLTree(AVLTree&& other) : _root(nullptr) {
    this->_root = other._root;
    this->copyTree(_root);
    other._root = nullptr;
  }

  // Copy assignment operator- perform a deep copy assignment
  AVLTree& operator=(const AVLTree& other) {
    if (this != &other) {
      this->_root = other._root;
      this->copyTree(_root);
    }
    return *this;
  }

  // Move assignment operator
  AVLTree& operator=(AVLTree&& other) {
    if (this != &other) {
      if (!this->empty()) {
        this->_root = nullptr;
      }
      this->_root = other._root;
      this->copyTree(_root);

      other._root = nullptr;
    }
    return *this;
  }

  // Public API
  void clear() {
    if (this->_root) {
      this->makeClearHelper(this->_root);
    }
    this->_root = nullptr;
  }

  void add(T val) {
    if (this->_root) {
      this->addHelper(this->_root, val);
    } else {
      this->_root = new AVLNode<T>(val);
    }
  }

  bool isEmpty() {
    return(this->_root == nullptr);
  }

  int height(AVLNode<T> *t) const {
    if (t == nullptr) {
      return -1;
    } else {
      return t->height;
    }
  }

  // The print functions take an *optional* ostream handle
  // Not providing one will have them default to std::cout (the terminal)
  void print(std::ostream& out = std::cout) {
    printInOrderHelper(this->_root);
  }

  void printInOrder(std::ostream& out = std::cout) {
    printInOrderHelper(this->_root, out);
  }

  void printRevOrder(std::ostream& out = std::cout) {
    printRevOrderHelper(this->_root, out);
  }

  void printPostOrder(std::ostream& out = std::cout) {
    printPostOrderHelper(this->_root, out);
  }

  void printPreOrder(std::ostream& out = std::cout) {
    printPreOrderHelper(this->_root, out);
  }

  void printMax(std::ostream& out = std::cout) {
    printMaxHelper(this->_root, out);
  }

  void printMin(std::ostream& out = std::cout) {
    printMinHelper(this->_root, out);
  }

  int treeHeight() {
    return heightHelper(this->_root);
  }

  void printMaxPath() {
    printMaxPathHelper(this->_root);
  }

  bool deleteValue(T value) {
    return this->deleteValueHelper(nullptr, this->_root, value);
  }

  bool contains(T value) {
    return containsHelper(this->_root, value);
  }
};

#endif  // CPT_S_223_PA3_SRC_AVL_TREE_H_
