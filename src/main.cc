// Copyright (C) 2020 Andrew Todd
#include <iostream>
#include <fstream>
#include <sstream>

#include "cancer_data.h"  // NOLINT
#include "avl_node.h"  // NOLINT
#include "avl_tree.h"  // NOLINT

// BORROWED BASE BST CODE FROM MA2 AND ADDED BALANCING

/*
What is the worst-case Big-O of the insert () algorithm for the AVL tree? Explain.
  It should be O(log(N)) because the tree should always be balanced.

What is the worst-case Big-O of the printInOrder () algorithm for the AVL tree? Explain.
  I think it's O(N) because you have to read every node in the tree.

What is the worst-case Big-O of the findMax () algorithm for the AVL tree? Explain.
  I think it would be O(N/2) because it only goes down the right subtree, which is half of the tree.
*/

// Read data from files into trees
void loadData(AVLTree<CancerData> *&tree, std::string file) {  // NOLINT
  std::fstream fin;
  std::string line, in_country, in_rate;
  fin.open(file);

  if (!fin.fail()) {
    while (std::getline(fin, line)) {
      std::istringstream iss(line);
      std::getline(iss, in_country, ',');
      std::getline(iss, in_rate);
      CancerData temp(in_country, std::stod(in_rate));
      tree->add(temp);
    }
  } else {
      std::cout << std::endl;
      std::cout << file << " could not be opened!\n";
      std::cout << std::endl;
  }
  fin.close();
}

int main() {
  // Strings with cancer data file names
  const std::string men_file = "mencancerdata2018.csv";
  const std::string women_file = "womencancerdata2018.csv";
  const std::string both_file = "bothcancerdata2018.csv";

  // Creating three trees for data
  AVLTree<CancerData> *men_tree = new AVLTree<CancerData>;
  AVLTree<CancerData> *women_tree = new AVLTree<CancerData>;
  AVLTree<CancerData> *both_tree = new AVLTree<CancerData>;

  // loading data into the trees
  loadData(men_tree, men_file);
  loadData(women_tree, women_file);
  loadData(both_tree, both_file);

  // Cout cancer rates
  std::cout << "Cancer Rates of Men and Women: " << std::endl;
  both_tree->printRevOrder();
  std::cout << std::endl;
  std::cout << std::endl;
  std::cout << "Cancer Rates of Men: " << std::endl;
  men_tree->printRevOrder();
  std::cout << std::endl;
  std::cout << std::endl;
  std::cout << "Cancer Rates of Women: " << std::endl;
  women_tree->printRevOrder();
  std::cout << std::endl;
  std::cout << std::endl;

  // Cout highest cancer rates
  std::cout << "Highest Cancer Rate for Men and Women: " << std::endl;
  both_tree->printMax();
  std::cout << std::endl;
  std::cout << "Highest Cancer Rate for Men: " << std::endl;
  men_tree->printMax();
  std::cout << std::endl;
  std::cout << "Highest Cancer Rate for Women: " << std::endl;
  women_tree->printMax();
  std::cout << std::endl;
  std::cout << std::endl;

  // Cout lowest cancer rates
  std::cout << "Lowest Cancer Rate for Men and Women: " << std::endl;
  both_tree->printMin();
  std::cout << std::endl;
  std::cout << "Lowest Cancer Rate for Men: " << std::endl;
  men_tree->printMin();
  std::cout << std::endl;
  std::cout << "Lowest Cancer Rate for Women: " << std::endl;
  women_tree->printMin();
  std::cout << std::endl;

  // Freeing all memory, confirmed no leaks with Valgrind
  men_tree->clear();
  women_tree->clear();
  both_tree->clear();
  delete men_tree;
  delete women_tree;
  delete both_tree;
  return 0;
}
